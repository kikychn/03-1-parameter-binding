package com.twuc.webApp.sources;

import javax.validation.constraints.*;

public class Contract {
    @NotNull
    private String contractId;
    @Size(min = 3, max = 8)
    private String contractName;
    @Min(1000)
    @Max(9999)
    private Integer yearOfBirth;
    @Email
    private String email;

    public Contract() {
    }

    public Contract(@Size(min = 3, max = 8) String contractName, @Min(1000) @Max(9999) Integer yearOfBirth, @Email String email) {
        this.contractName = contractName;
        this.yearOfBirth = yearOfBirth;
        this.email = email;
    }

    public Contract(@NotNull String contractId, @Size(min = 3, max = 8) String contractName, @Min(1000) @Max(9999) Integer yearOfBirth, @Email String email) {
        this.contractId = contractId;
        this.contractName = contractName;
        this.yearOfBirth = yearOfBirth;
        this.email = email;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public String getContractId() {
        return contractId;
    }

    public Integer getYearOfBirth() {
        return yearOfBirth;
    }

    public String getEmail() {
        return email;
    }
}
