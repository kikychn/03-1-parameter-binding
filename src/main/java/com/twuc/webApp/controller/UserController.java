package com.twuc.webApp.controller;

import com.twuc.webApp.model.DateTime;
import com.twuc.webApp.sources.Contract;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.ZonedDateTime;
import java.util.Collection;

@RestController
@RequestMapping("/api")
public class UserController {

    @GetMapping("/users/{userId}")
    public String getUserId(@PathVariable Integer userId) {
        return "user " + userId;
    }

    @GetMapping("/students/{studentId}")
    public String getStudentId(@PathVariable int studentId) {
        return "student " + studentId;
    }

    @GetMapping("/users/{userId}/books/{bookId}")
    public String getUserId(@PathVariable Integer userId, @PathVariable Integer bookId) {
        return "user " + userId + " book " + bookId;
    }

    @PostMapping("/user")
    public String getRequestParam(@RequestParam(defaultValue = "zpp") String username, @RequestParam Integer age) {
        return username + " " + age;
    }

    @PostMapping("/books")
    public String getRequestParamWhichIsCollection(@RequestParam Collection<Integer> book) {
        return book.toString();
    }

    @PostMapping("/datetimes")
    public ZonedDateTime getDateTime(@RequestBody DateTime time) {
        return time.getDateTime();
    }

    @PostMapping("/contracts")
    public Contract getContract(@RequestBody @Valid Contract contract) {
        return contract;
    }

}
