package com.twuc.webApp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.sources.Contract;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {
    ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Autowired
    private MockMvc mockMvc;

    @Test
    void request_param_is_defined_integer() throws Exception {
        mockMvc.perform(get("/api/users/1"))
                .andExpect(content().string("user 1"))
                .andExpect(status().isOk());
    }

    @Test
    void request_param_is_defined_int() throws Exception {
        mockMvc.perform(get("/api/students/1"))
                .andExpect(content().string("student 1"))
                .andExpect(status().isOk());
    }

    @Test
    void multiple_request_params_are_defined() throws Exception {
        mockMvc.perform(get("/api/users/1/books/2"))
                .andExpect(content().string("user 1 book 2"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_request_param_using_postmapping() throws Exception {
        mockMvc.perform(post("/api/user")
                .param("username", "zpp")
                .param("age", "18"))
                .andExpect(content().string("zpp 18"));
    }

    @Test
    void should_return_request_param_using_postmapping_without_enough_param() throws Exception {
        mockMvc.perform(post("/api/user")
                .param("age", "18"))
                .andExpect(content().string("zpp 18"));
    }

    @Test
    void should_return_request_param_using_postmapping_with_collection_params() throws Exception {
        mockMvc.perform(post("/api/books")
                .param("book", "1", "2", "3"))
                .andExpect(content().string("[1, 2, 3]"));
    }

    @Test
    void should_serialize_Contract() throws JsonProcessingException {
        Contract contract = new Contract("1", "hello",1999,"meimei@163.com");
        String json = objectMapper.writeValueAsString(contract);
        assertEquals("{\"contractId\":\"1\",\"contractName\":\"hello\",\"yearOfBirth\":1999,\"email\":\"meimei@163.com\"}", json);
    }

    @Test
    void should_serialize_Contract_using_constructor() throws JsonProcessingException {
        Contract contract = new Contract("1", "hello",1999,"meimei@163.com");
        String json = objectMapper.writeValueAsString(contract);
        assertEquals("{\"contractId\":\"1\",\"contractName\":\"hello\",\"yearOfBirth\":1999,\"email\":\"meimei@163.com\"}", json);
    }

    @Test
    void should_deserialize_Contract_using_constructor() throws IOException {
        String json = "{\"contractId\":\"1\",\"contractName\":\"hello\",\"yearOfBirth\":1999,\"email\":\"meimei@163.com\"}";
        Contract contract = objectMapper.readValue(json, Contract.class);
        assertEquals("hello", contract.getContractName());
    }

    @Test
    void should_serialize_datatime() throws Exception {
        String json = "{ \"dateTime\": \"2019-10-01T10:00:00Z\" }";
        mockMvc.perform(post("/api/datetimes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().string("\"2019-10-01T10:00:00Z\""));
    }

    @Test
    void should_serialize_datatime_with_not_format() throws Exception {
        String json = "{ \"dateTime\": \"2019-10-01\" }";
        mockMvc.perform(post("/api/datetimes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_contract_when_request_param_with_contractId() throws Exception {
        Contract contract = new Contract("1", "hello",1999,"meimei@163.com");
        mockMvc.perform(post("/api/contracts")
                .content(objectMapper.writeValueAsString(contract))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(content().string("{\"contractId\":\"1\",\"contractName\":\"hello\",\"yearOfBirth\":1999,\"email\":\"meimei@163.com\"}"));
    }

    @Test
    void should_return_bad_request_when_request_param_without_contractId() throws Exception {
        Contract contract = new Contract("hello",1999,"meimei@163.com");
        mockMvc.perform(post("/api/contracts")
                .content(objectMapper.writeValueAsString(contract))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_bad_request_when_request_contractname_length_is_illegal() throws Exception {
        Contract contract = new Contract("1", "hello",1999,"meimei@163.com");
        contract.setContractName("hellohello");
        mockMvc.perform(post("/api/contracts")
                .content(objectMapper.writeValueAsString(contract))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_bad_request_when_request_yearOfBirth_is_illegal() throws Exception {
        Contract contract = new Contract("1", "hello",999,"meimei@163.com");
        mockMvc.perform(post("/api/contracts")
                .content(objectMapper.writeValueAsString(contract))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_bad_request_when_request_email_is_illegal() throws Exception {
        Contract contract = new Contract("1", "hello",999,"meimei163.com");
        mockMvc.perform(post("/api/contracts")
                .content(objectMapper.writeValueAsString(contract))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

}
